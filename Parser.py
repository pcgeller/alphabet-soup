"""
alphabet coding challenge for Enlighten
Created on: 18 July 2021
Author: Paul Gellerman

Parser class that takes the raw file and creates puzzle parts.
"""

import config as cfg
from os.path import join

class Parser:
    def __init__(self, input_file=cfg.input_file_path):
        """
        This class creates a simple parser that reads in an input file, separates it into the word_grid and the words
        to be found.  Init the parser with a file.  After initialization the parser object will store the results in itself.

        :param str input_file: complete path for input file.

        """
        self.word_grid = []
        self.word_grid_dim = None
        self.word_searches = []
        self.file_data_raw = []

        file_path = join(cfg.home_path, input_file)
        with open(file_path, 'r') as f:
            self.file_data_raw = f.readlines()

    def clean(self, cleaned_characters = ['\n', ' ']):
        """
        Clean the data by removing unwanted characters

        :param list cleaned_characters: characters that should be removed form the raw data.
        """
        #removed_characters = ['\n', ' ']
        for row_num, line in enumerate(self.file_data_raw):
            cleaned_row = [c for c in line if c not in cleaned_characters]
            if row_num == 0:
                self.word_grid_dim = int(line[0])
            elif row_num <= self.word_grid_dim:
                self.word_grid.append(cleaned_row)
            else:
                self.word_searches.append(cleaned_row)


