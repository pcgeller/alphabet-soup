# Alphabet Soup Solver

> Solution for : https://gitlab.com/enlighten-challenge/alphabet-soup

## Requirements

Built on Python 3.8.10 and uses standard libraries.  

To run:
1.  Download repository to your local machine.  
2.  Edit config.py to specify the home path where your input file is and the file name.
3.  In terminal run `python ./Solver.py`
    1.  You may need to modify the file to make it executable `chmod +x Solver.py`

