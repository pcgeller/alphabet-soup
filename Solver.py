#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
alphabet coding challenge for Enlighten
Created on: 18 July 2021
Author: Paul Gellerman

Solver that takes puzzle parts; word grid and list of words to be found.  Finds the words and prints their start
and ending positions.
"""

class Solver:
    def __init__(self, word_grid, word_searches):
        """
        Solver that takes a word grid and a list of words to be found.

        :param list word_grid: list of lists with the outer list containing the rows.  Inner list containing the columns
        for that row.
        :param list word_searches: List of lists for words to be found.  Outer list contains each word, inner list
        contains each letter.
        """
        self.found_words = []
        self.word_grid = word_grid
        self.word_searches = word_searches
        self.word_grid_dim = [(len(self.word_grid) - 1), (len(self.word_grid[0]) - 1)]

    def check_word(self, word, start_position):
        """
        Called after a match on the first letter from the main control flow loop.
        Checks a word given the initial starting position.  If the word is length 1 then it returns immediately.
        Otherwise it checks through different directions for the word on the word grid.  If the direction would put the
        word out of the index bounds then it skips checking the whole word.  Finally it increments by the direction to
        check every letter in the word against the grid.

        :param list word: List of letters that make up the word.
        :param list start_position: Initial starting position on the word grid.
        :return list word: The word that was found.
        :return list start_position: Starting position in the word grid for the word.
        :return list letter_position: Ending position in the word grid for the word.
        """
        word_length = len(word)

        if word_length == 1:
            return word, start_position, start_position

        # Check directions that don't go out of bounds.
        for row_dir in [1, 0, -1]:
            if ((word_length - 1) * row_dir) + start_position[0] > self.word_grid_dim[0] or \
                    ((word_length - 1) * row_dir) + start_position[0] < 0:
                continue

            for col_dir in [1,0,-1]:
                if ((word_length - 1) * col_dir) + start_position[1] > self.word_grid_dim[1] or \
                        ((word_length - 1) * col_dir) + start_position[1] < 0:
                    continue
                #Has valid direction, fits within index boundaries for both row and column so check the rest of the word.

                for counter, character in enumerate(word):
                    position_adjustment = [counter * x for x in [row_dir, col_dir]]
                    letter_position = [a + b for a, b in zip(start_position, position_adjustment)]
                    if character == self.word_grid[letter_position[0]][letter_position[1]]:
                        if counter == (len(word) - 1):
                            self.word_searches.remove(word)
                            return word, start_position, letter_position
                    else:
                        break
        return None, None, None

                    #print("end of solver")


    def solve(self):
        """
        Loops over each row in the word grid and then each column within the row.  If a letter matches the first letter
        in the word_searches then it calls check_word to see if there is a complete match.

        Appends found words to self.found_words
        """
        # Loop over rows.
        for row_index in range(len(self.word_grid)):
            # Loop over columns
            for col_index in range(len(self.word_grid[row_index])):
                position = [row_index, col_index]
                position_value = self.word_grid[position[0]][position[1]]
                #Loop over words to be found
                for word in self.word_searches:
                    if word[0] == position_value:
                        found_word, found_word_start, found_word_end = self.check_word(word, position)
                        if found_word is None:
                            continue
                        else:
                            self.found_words.append([found_word, found_word_start, found_word_end])

    def show_results(self):
        """
        Print the results found from solving the word grid.
        """
        for result in self.found_words:
            print(f"{''.join(result[0])} {result[1][0]}:{result[1][1]} {result[2][0]}:{result[2][1]}")

if __name__ == '__main__':
    import Parser
    import Solver
    p = Parser.Parser()
    p.clean()

    s = Solver.Solver(p.word_grid, p.word_searches)
    s.solve()
    s.show_results()













